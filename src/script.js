//1
const featureConst = document.getElementsByClassName('feature');
console.log(featureConst);

const featureConst2 = document.querySelector('.feature');
featureConst2.style.textAlign = 'center';
console.log(featureConst2);

const featureConst3 = document.getElementsByTagName('span');
console.log(featureConst3);

//2
const textSwap = document.getElementsByClassName('textSwap');
textSwap.textContent = "Awesome feature";
console.log(textSwap);

//3--Для одного.
const textAdd = document.querySelector('.feature-title-1');
const text = '!';
textAdd.innerText += text;

//--Для списков и т.п.
const textAdd2 = document.getElementsByClassName('feature-title-2');
Array.from(textAdd2).forEach(element => {
    element.innerText += text;
})
